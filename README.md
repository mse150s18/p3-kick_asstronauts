P3_Kick_Asstronauts were given the task to manipulate matrices to solve for integrals of graphs as shown in the challenges below. During the 4-week period, the challenges will change to accomodate the furthering completion of the overall coding. This README.md file explains how to run the coding we have put forth! Thank you!

* Summary
	* This code will run through an excel file and then from that file be able to print out graphs of the different data. The excel file nbeing ran hsa three different sets of x and y data. This will create three different graphs. The code is used through importing a variety of utilities from peakutils. These utilities are in the peakutilsCode.py. We then use pandas to plot the actual graphs and were able to find the peaks from using the indexes. These will go through all the peaks and find any peak that is higher than a certain thresth that is astablished. We did this methd for each of the sets of data. We realized at the end of this that we did a recursion method so we had to repeat this code  when we could have just done one set and ran the data through that one code. 
	* This code is not the most fluent and could be designed differently. If we did this for a while, we think we could figure out a better code to use that would run ,ore smoothly and could be used for any excel file.
	
	
* How to run code?
	* To run the code, the user needs to run the p3_code.py through python. Ex: python p3_code.py The user can also find the graphs in the folder called Plots. 
	
* Steps to getting to the code
	* The user needs to go trhough the p3-kick_asstronauts folder and then enter into the p3-kick_asstronauts again. Once they are in the folder, the user can find the code in the p3_code.py  