import glob
import matplotlib.pyplot as plt
import sys
import os
import xlwt
import xlrd
import xlsxwriter
import numpy as np
import pandas as pd
import scipy 
from scipy import signal
import peakutilsCode as pu




#Import Data From Excel

def plots(data):
	min = [.2, .3, .55]
	data.columns = ['x1','y1','x2','y2','x3','y3']
	

	#First Plot#
	heights = pu.indexes(data['y1'], thres = min[0])
	pu.plot(data['x1'], data['y1'], heights)
	plt.xlabel('Binding Energy, eV')
	plt.ylabel('Intensity, counts/sec')
	title = 'Plot_1'
	plt.suptitle(title)
	directory = 'plots'
	if not os.path.exists(directory):
		os.makedirs(directory)
	plt.savefig(directory + '/' + title)	
	plt.show
	plt.clf()

	#Plot 2#
	heights = pu.indexes(data['y2'], thres = min[1])
	pu.plot(data['x2'], data['y2'], heights)
	plt.xlabel('Binding Energy, eV')
	plt.ylabel('Intensity, counts/sec')
	title = 'Plot_2'
	plt.suptitle(title)
	directory = 'plots'
	plt.savefig(directory + '/' + title)	
	plt.show
	plt.clf()

	#Plot 3#
	heights = pu.indexes(data['y3'], thres =  min[2])
	pu.plot(data['x3'], data['y3'], heights)
	plt.xlabel('Binding Energy, eV')
	plt.ylabel('Intensity, counts/sec')
	title = 'Plot_3'
	plt.suptitle(title)
	directory = 'plots'
	plt.savefig(directory + '/' + title)	
	plt.show
	plt.clf()

	return

data=pd.read_excel("xps-data.xls",skiprows=1)
plots(data)