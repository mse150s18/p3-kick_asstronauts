This will be our tutorial file
We need to explain our favorite lines of code used in the project.
Each entry in this file should begin with a line of code, followed by an explanation in English about what the python interpreter does when it executes the line
**The lines of code do not need to be used in our final code. They can be mess ups, just explain why they did not work**
***Focus on what the code is actually doing, not what its intent is/ walk through what python is doing in order to preform the function***

## Cole Fowler ##
-Work on the Grain Size Analysis

1) labeled, nr_objects = ndimage.label(Stonef > T) # `Stone[:,:,0]>T` for print ("# Items is %d" % nr_objects)

This takes the image that was labeled 'Stone' and resizes what to print out

2) import Lively from Lively import ndimage = Lively.misc.imread('Stone.tif')

This Imports the given image from the 'Lively' spot of the rock image

3) color threshold, threshold = 100 for j in range(27): for i in range(2070):
		if image[j*45,i] <= threshold and image[j*45,i+1] > threshold:

This gives me my sample area that I would be analyzing in a picture for the grain sizes

4) blacks = 0 totalpixels = 2000*1200

This code gives me the capability to label a certain color scheme and the area from which it takes the color scheme into effect

5) area = 1.30*10^-6 #square meters, print(str(grains) + ' as 1.30e-6 sq m'), print(str(grains/(area)) + 'grains^2')

sample area = 1.30*10^-6 sq meters measured for printing grain sizes

6) grainsize = 1 / (percentage*grains/(area)), print('avg grainsize = ' + str(grainsize) + ' m^2')

Sample area is printed based on the grainsizes in the given percentage area of grains

7) plt.plot(x, m*x + b, label="fit"), plt.plot(x, simulated_data, label="sim data", alpha=0.5), plt.legend(), plt.savefig("test.png")

Sample coding puts multiple graphs on top of one another. This allows for cross analysis on the multiple graphs. This cutes the uselss portions of the MatLab graph.

8) params = curve_fit(func, x, simulated_data), m, b = params[0]

This code sets the sample into particular parameters that allow for multiple graphs to be stacked on the same page.

## Monica Leibowitz ##

1) xvals = np.array([718.2,604.0,573.4,374.4,368.4,97.8,58.8,5.2]) yvals = np.array([52865.483,88645.206,115573.8636,269331.4631,343597.1591,13724.1122,16111.8608,35518.3594])

These two lines of code list 8 numbers that are passed as an argument to the array function inside np. This converts the list of numbers into two arrays, storing them in the variables "xvals" and "yvals".

2) plt.plot(xvals,yvals), plt.show()

Using the arrays stored in the variables from 1), matplotlib reads the arrays and plots a graph with xvals on the x axis and yvals on the y axis. Then, the plot generated through matplotlib is displayed. This did not work as intended because I misunderstood the data and accidently just plotted the peaks.

3) df=pd.read_excel("xps-data.xls", skiprows=1)

Using panadas and the read_excel function inside the df variable, the excel file "xps-data.xls" is imported, and its data is saved to the variable "df"."skiprows=1" skips the first row of data in the file, ignoring the Problem xx Data titles.

4) print(df.head())

This takes the variable df with the excel file "xps-data.xls" saved to it and prints the first 5 lines of data in the file using the "head" function which belongs to the df variable.

5) plt.xlabel("Binding Energy (eV)")

This uses matplotlib to take an array stored as the x values of a plot and labels the values as "Binding Energy (eV)". 
 
6) import numpy as np

This line of code imports the package "numpy" into the konsole and stores it as a variable under the name "np".

7) plot(xvals, yvals, color='green', marker='*', linestyle='dashed')

This line of code uses the package matplotlib to plot the arrays stored in the "xvals" and "yvals" variables and set the produced line to be green dashed stars using the parameters listed.  

8) x = np.linspace(0,100)

Uses the package "numpy" stored to the variable "np" to access the function "linspace". This function takes the specified interval and outputs an array of 100 datapoints, all evenly spaced.

9) noise = np.random.normal(0, 2, len(x))

This line of code uses the randomization function within the numpy package to draw random samples from the parameterized normal distribution function. len(x) takes the argument 'x' and returns the length of the argument. All of this data is then stored within the variable "noise".

10) simulated_data  = x*3 + 5 + noise

This line of code creates a variable by the name of "simulated_data" and stores the expression "x*3 + 5 + noise" within it. "Noise" is another variable defined in 9) and 'x' is an argument also defined in 9). 
 
### Olivia Maryon ###

1) directory = 'plots'

This line of code will create a directory called plots

2) plt.show

This code will show the plots that are made

3) pu.plot(data['x1'], data['y1'], heights)

This code will create a plot that reads an x and a y value but will have a thresh hold of a certain amount so to find the peaks it doesn't use the very small peaks

4) plt.clf()

This will clear the current plot so if you are making multply plots they do not over lap one another

5) heights = pu.indexes(data['y1'], thres = min[0])

This will allow the peaks to not be found any lower than a certain amount on the y axis

6) if not os.path.exists(directory):
              os.makedirs(directory)

This code will check and if there is no directory then it will create a directory to stroy what the user needs

7) plt.savefig(directory + '/' + title)

This will save the plot in the created directory and save it with the title given

8) data=pd.read_excel("xps-data.xls",skiprows=1)

This will read the excel file and skip the first row

9) def plot(x, y, ind):

This will define what each plot will look like through the entire loop

10) thres = thres * (np.max(y) - np.min(y)) + np.min(y)

This code will find the min thres(min peak) that is allowed in the graph

### Chris Jones ###

1) def plots(data):

This creates a user defined function that can be called upon later on in the code.

2)  plt.xlabel('Binding Energy, eV')

This gives a name to the x-axis on the graph

3) import peakutilsCode as pu

This imports a separate python file we created for this project.  It can be called upon later in this code to execute the code it contains.

4) if not os.path.exists(directory):
                os.makedirs(directory)

This code will create a directory to save the graphs in if one does not already exist.

5)data=pd.read_excel("xps-data.xls",skiprows=1)

This code opens and reads an excel file while assigning its contents to a variable called data

6) plots(data)

This code calls upon a user defined funciton from an earlier point in the code.  It runs the code within that functon on the variable called data.

7) pu.plot(data['x1'], data['y1'], heights)

This code uses the code contained in a separate python file that we imported earlier.  It sits within our user defined function of plots.

8)plt.savefig(directory + '/' + title)

This line of code saves the figure created while giving it a unique name from the other plots created.

9) plt.show

This code shows the visual representation of the plot, so the graph will automatically load when the user runs the code.

#10) title = 'Plot_3'#

This code gives a title to the graph, which is important later when it saves the 3 separate graphs as individual files.


# Greg Noble #

1) img=mpimg.imread('png flile inserted that you want code to read')
This code that I like is 

2) matplotlib.image
This import gives the capability of writing a command code to open a picture file.  

2.) img=mpimg.imread('png flile inserted that you want code to read')
This code that I like is used for opening png files that are saved to the computer from a source. mpimg.imread takes the .png file that is inserted into the parentheses and matplotlib.image opens it.  

3) print(df.head()) # The command df.head prints first five lines of data frame. a data frame is a special thing with panda. This code is helpful to not overload the computer with data. 

4)df=pd.read_excel("../xps-data.xls", skiprows=1) I like two things about this code. The first reason is that read_excel is code from the pandas library that reads the xls file. The other is inside of the command "../" I like this because it allows for opening files from a directory above a directory. 

5)[maxtab, mintab] = peakdet(x, 0.5); This was found on a site that I was going to implement into my code. The peakdet call finds the maximum and minimum values of X. It is able to do this in noisy data because it differentiates between neighboring maximums and minimums. 

6)
x2 =    df['Binding energy (eV).1']
y2 =    df['Intensity (counts/second).1']
These lines of code seperate the different x and y variable of different sets. the x2,y2 assigns the x and y values. The brackets and apostrophies [''] take in name that is assigned to the data in the imported file and seperates from the others so you don't get multiple graphs superimposed. the df takes the information and gives it to matplot and then the graphs are allowed to be put out seperately.  

7) plt.plot(x1,y1) This plots the defined x2 and y2 information that were assigned to data. 

8) noise = np.random.normal(0, 2, len(x1)) This code is helpful for removing noise in the data. When data has a lot of noise (meaning a jagged line rather than a smooth curve ), it can be difficlult to find a maximum that is close to but seperate from those jagged peaks. 

9) format_whatever_data_you_want_formated  = lens.Each(). This code has the potential to take in a string of commands. 

10) plt.show() plt show allows to bring up an image of the data shown in a graph. 
