import pandas as pd
import matplotlib.pyplot as plt

df=pd.read_excel("../xps-data.xls", skiprows=1)
print(df.head()) # The command df.head prints first five lines of data frame. a data frame is a special thing with panda

df.plot()
plt.show()

#Want to get at data from df, for particular data sets
#For example: 
#print(df['Binding energy (ev).1'])

x1 = 	df['Binding energy (eV)']
y1 = 	df['Intensity (counts/second)']

plt.clf()
plt.plot(x1,y1)
plt.show()


x2 =	df['Binding energy (eV).1']
y2 =	df['Intensity (counts/second).1']
plt.clf()
plt.plot(x2,y2)
plt.show()

x3 =	df['Binding energy (eV).2']
y3 =	df['Intensity (counts/second).2']
plt.clf()
plt.plot(x3,y3)
plt.show()

# make a list by cycling through the colors you care about
# to match the length of your data.
#graphed_colors = list(islice(cycle(['b', 'r', 'g'])


