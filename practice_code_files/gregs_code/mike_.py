## IMPORTANT ## The following code has my code that plots the graphs (only for the first graph inputed so far) mixed into mikes code for finding peaks that was provided on bit bucket. I was able to get help from him in class today so it should be on the right track. The current issue that I am running into is getting rid of columns that I don't want (i.e. Binding energy (eV).1,Binding energy (eV).2 and their corresponding y columns "Intensity"). Mike suggested that I google "pandas column select" and find out how to select specific columns out. I've included sites at the bottom that i found usefull. the link to mikes video and code is also provided. 

# Short Summary paragraph from above:
# This file contains mikes code with some of mine inputed. My code is copied and pasted at the bottom and hash taged out. resources included at bottom. 



import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import pandas as pd

df=pd.read_excel("../xps-data.xls", skiprows=1)
print(df.head())

#df.plot()
#plt.show()

x1 = df['Binding energy (eV)']
y1 = df['Intensity (counts/second)']

#plt.clf()
#plt.plot(x1,y1)
#plt.plot(x1,y1)

#####x = np.linspace(0,100)
#noise = np.random.normal(0, 2, len(x1))
simulated_data  = y1

def func(x, m, b):
    return m*x + b

params = curve_fit(func, x1, simulated_data)
m, b = params[0]
print("m=", m, " b=", b)

plt.plot(x1, m*x1 + b, label="fit")
plt.plot(x1, simulated_data, label="sim data", alpha=0.5)
plt.legend()
plt.savefig("test.png")

print(x1)


#import pandas as pd
#import matplotlib.pyplot as plt

#df=pd.read_excel("../xps-data.xls", skiprows=1)
#print(df.head()) # The command df.head prints first five lines of data frame. a data frame is a special thing with panda

#df.plot()
#plt.show()

#Want to get at data from df, for particular data sets
#For example: 
#print(df['Binding energy (ev).1'])

#x1 =    df['Binding energy (eV)']
#y1 =    df['Intensity (counts/second)']

#plt.clf()
#plt.plot(x1,y1)
#plt.show()


#x2 =    df['Binding energy (eV).1']
#y2 =    df['Intensity (counts/second).1']
#plt.clf()
#plt.plot(x2,y2)
#plt.show()

#x3 =    df['Binding energy (eV).2']
#y3 =    df['Intensity (counts/second).2']
#plt.clf()
#plt.plot(x3,y3)
#plt.show()


######### FROM GOOGLING "Pandas column select"
#### https://www.google.com/search?q=pandas+column+select&ie=utf-8&oe=utf-8
#### https://stackoverflow.com/questions/11285613/selecting-columns-in-a-pandas-dataframe
#### https://stackoverflow.com/questions/10665889/how-to-take-column-slices-of-dataframe-in-pandas
#### https://pandas.pydata.org/pandas-docs/stable/indexing.html

######### FROM MIKE TO CLASS FROM BIT BUCKET FOR FITTING A CURVE WITH PYTHON
#### https://gist.github.com/mikemhenry/80be168021a85f762ac28436dbbb4836
