plt.show()

#Want to get at data from df, for particular data sets
#For example: 
#print(df['Binding energy (ev).1'])

x1 = 	df['Binding energy (eV)']
y1 = 	df['Intensity (counts/second)']

plt.clf()
plt.plot(x1,y1)
plt.xlabel("Binding Energy (eV)")
plt.ylabel("Intensity (kcount/second)")
plt.show()


x2 =	df['Binding energy (eV).1']
y2 =	df['Intensity (counts/second).1']
plt.clf()
plt.plot(x2,y2)
plt.xlabel("Binding Energy (eV)")
plt.ylabel("Intensity (kcount/second)")
plt.show()

x3 =	df['Binding energy (eV).2']
y3 =	df['Intensity (counts/second).2']
plt.clf()
plt.plot(x,y)
plt.xlabel("Binding Energy (eV)")
plt.ylabel("Intensity (kcount/second)")
plt.show()

# make a list by cycling through the colors you care about
# to match the length of your data.
#graphed_colors = list(islice(cycle(['b', 'r', 'g'])
          
