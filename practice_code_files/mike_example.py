import matplotlib 
matplotlib.use('agg')
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

x = np.linspace(0,100)
noise = np.random.normal(0, 2, len(x))
simulated_data  = x*3 + 5 + noise

def func(x, m, b):
    return m*x + b

params = curve_fit(func, x, simulated_data)
m, b = params[0]
print("m=", m, " b=", b)

plt.plot(x, m*x + b, label="fit")
plt.plot(x, simulated_data, label="sim data", alpha=0.5)
plt.legend()
plt.savefig("test.png")




#THE FOLLOWING WAS COPIED FROM GREG'S CODE
#import pandas as pd
#import matplotlib.pyplot as plt

#df=pd.read_excel("xps-data.xls", skiprows=1)
#print(df.head()) # The command df.head prints first five lines of data frame. a data frame is a special thing with panda

#df.plot()
#plt.show()

#Want to get at data from df, for particular data sets
#For example: 
#print(df['Binding energy (ev).1'])

#x1 =    df['Binding energy (eV)']
#y1 =    df['Intensity (counts/second)']

#plt.clf()
#plt.plot(x1,y1)
#plt.show()


#x2 =    df['Binding energy (eV).1']
#y2 =    df['Intensity (counts/second).1']
#plt.clf()
#plt.plot(x2,y2)
#plt.show()

#x3 =    df['Binding energy (eV).2']
#y3 =    df['Intensity (counts/second).2']
#plt.clf()
#plt.plot(x3,y3)
#plt.show()

import pandas as pd
import matplotlib.pyplot as plt

df=pd.read_excel("xps-data.xls", skiprows=1)
print(df.head()) # The command df.head prints first five lines of data frame. a data frame is a special thing with panda

df.plot()
plt.xlabel('Binding Energy (eV)')
plt.ylabel('Intensity (kcount/second)')
plt.show()

#Want to get at data from df, for particular data sets
#For example: 
#print(df['Binding energy (ev).1'])

x1 =    df['Binding energy (eV)']
y1 =    df['Intensity (counts/second)']

plt.clf()
plt.plot(x1,y1)
plt.xlabel("Binding Energy (eV)")
plt.ylabel("Intensity (kcount/second)")
plt.show()


x2 =    df['Binding energy (eV).1']
y2 =    df['Intensity (counts/second).1']
plt.clf()
plt.plot(x2,y2)
plt.xlabel("Binding Energy (eV)")
plt.ylabel("Intensity (kcount/second)")
plt.show()

x3 =    df['Binding energy (eV).2']
y3 =    df['Intensity (counts/second).2']
plt.clf()
plt.plot(x3,y3)
plt.xlabel("Binding Energy (eV)")
plt.ylabel("Intensity (kcount/second)")
plt.show()

