import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy as sp
import warnings
from scipy import signal
from scipy import optimize
from scipy.integrate import simps

#CODE THAT DEFINES THE X AND Y VALUES AND PLOTS FOR EACH SAMPLE#

#Import Data From Excel
data=pd.read_excel("xps-data.xls",skiprows=1)
#print(data.columns) #Shows the column header names of the excel file

#ASSIGNING VARIABLES TO EACH COLUMN OF THE SPREADSHEET:
#SAMPLE 1
sample1ax=data['Binding energy (eV)'] #Sample 1a binding energy
sample1ay=data['Intensity (counts/second)'] #Sample 1a intensity
sample1a_Array = np.array(sample1ay)

#SAMPLE 2
sample1bx=data['Binding energy (eV).1'] #sample 1b binding energy
sample1by=data['Intensity (counts/second).1'] #sample 1b intensity
sample1b_Array = np.array(sample1by)

#SAMPLE 3
sample2x=data['Binding energy (eV).2'] #sample 2 binding energy
sample2y=data['Intensity (counts/second).2'] #sample 2 intensity
sample2_Array= np.array(sample2y)

#Plots the data from each set of samples and prints a graph#
sample1a=plt.plot(sample1ax,sample1ay)
#plt.show(sample1a)
sample1b=plt.plot(sample1bx,sample1by)
#plt.show(sample1b)
sample2=plt.plot(sample2x,sample2y)
#plt.show(sample2)

#CODE TO FIND PEAKS#

eps = np.finfo(float).eps

def  indexes(y, thres=0.3, min_dist=1):

	if isinstance(sample1ay, np.ndarray) and np.issubdtype(sample1ay.dtype, np.unsignedinteger):
        	raise ValueError("y must be signed")

	thres = thres * (np.max(y) - np.min(y)) + np.min(y)
	min_dist = int(min_dist)
	dy = np.diff(y)
	zeros,=np.where(dy == 0)

	if len(zeros) == len(y) - 1:
		return np.array([])
	
	while len(zeros):
		zerosr = np.hstack([dy[1:], 0.])
		zerosl = np.hstack([0., dy[:-1]])
		
		dy[zeros]=zerosr[zeros]
		zeros,=np.where(dy==0)

		dy[zeros]=zerosl[zeros]
		zeros,=np.where(dy == 0)

	peaks = np.where((np.hstack([dy, 0.]) < 0.)
		& (np.hstack([0., dy]) > 0.)
		& (y > thres))[0]

	if peaks.size > 1 and min_dist > 1:
        	highest = peaks[np.argsort(y[peaks])][::-1]
	        rem = np.ones(y.size, dtype=bool)
        	rem[peaks] = False

	        for peak in highest:
        	    if not rem[peak]:
                	sl = slice(max(0, peak - min_dist), peak + min_dist + 1)
	                rem[sl] = True
        	        rem[peak] = False

	peaks = np.arange(y.size)
	
	return peaks	
	print (peaks)


def centroid(x, y):
	return np.sum(x * y) / np.sum(y)

def gaussian (x, ampl, center, dev):
	return ampl * np.exp(-(x - float(center)) ** 2 / (2.0 * dev ** 2 + eps))

def gaussian_fit (x, y, center_only=True):
	if len(x) < 3:
		raise RuntimeError("At least 3 points required for Gaussian fitting")
	
	initial = [np.max(y), x[0], (x[1] - x[0]) * 5]
	params, pcov = optimize.curve_fit(gaussian, x, y, initial)

	if center_only:
        	return params[1]
	else:
        	return params

#def interpolate(x, y, ind=None, width=10, func=gaussian_fit):
#	assert x.shape == y.shape
#	if ind is None:
#		ind = indexes(y)
	
#	out = []

#	for i in ind:
#		slice_ = slice(i - width, i + width + 1)

#		try:
#			best_idx = func(x[slice_], y[slice_])


#		return np.array(out)


q = indexes(sample1ay)
#print(q)

cent = centroid(sample1ax,sample1ay)
#print(cent)

gaus = gaussian(cent, 1, 1, 1,)
print(gaus)

gaussian_fit(sample1ax, sample1ay)
#interpolate(sample1ax, sample1ay)








	

















