#in order to read the xps-data.xls use this line of code 
import pandas as pd
import matplotlib.pyplot as plt

df=pd.read_excel("../xps-data.xls", skiprows=1)
print(df.head()) # The command df.head prints first five lines of data frame. a data frame is a special thing with panda
# In order to view type command into Bash $ python readexcel.py
df.plot()
plt.show()

