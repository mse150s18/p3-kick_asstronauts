https://bitbucket.org/lucashnegri/peakutils/src/e286ee206ee8e9bf2ec41b2e14a6e86a88ce7fe4/peakutils/peak.py?at=master&fileviewer=file-view-default

^^This link shows the code and functions of peakutils.  We should be able to pick out the parts that are helpful and apply it to our code.

For this project we may have to find how to do a lot of these things on Google. If you come across a website that has some useful and/or interesting code that applies to our project post a link to it here, and a quick summary of what was helpful if you want.

1. 
    https://plot.ly/python/peak-finding/


2. https://gist.github.com/endolith/250860 



3. April 31, 2018: I was confused at where to start with the project when I came in early today. Mike showed me a cool website that takes you through tutorials like we have been working on in class for the majority of the semester. steps for accessing the tutorials that are useful for figuring things out for this project are as follows..
	a.) http://www.datacarpentry.org/lessons/#ecology-workshop
	b.) Under workshop materials, click ecology curriculum
	c.) you will see the lessons available. Click on the site that you want to access for a tutorial.   



4. Professor Jankowski introducing jypyter sympy+tutorial command steps.
	a.) $ cd p3-start
	b.) $ jupiter notebook
	c.) The different sympy commands are shown. In order to get an output, click on box next to In[n]: and hit shift+enger to get an out[n]: .....

5.) PANDAS... Mike showed me this page this morning for using pandas to open an excel file.  



5. PANDAS FOR READING AN EXCEL
	a.) https://pandas.pydata.org/pandas-docs/stable/generated/pandas.read_excel.html
	b.) From this site you will see on the left hand side in the table of contents 10 minutes to pandas. I haven't checked this out yet but I guess it is a short tutorial on using pandas. 


6. https://www.dataquest.io/blog/pandas-python-tutorial/ - Talks about a lot of the data management techniques for Pandas

7. Videos on software computing describing commands from class
	REGULAR EXPRESSIONS
		# http://v4.software-carpentry.org/regexp/intro.html
	#1 the symbol '*'  matches any number of characters. Example: '*.txt' is a pattern that strings can match to anything that is a .txt.
	#2 the sumbol '|' is an or symbol. For example:
		import re
		for r in readings
		if re.search('06|07', r)
		  print r
		#this will select records that are in 06 OR 07 in records. First argument is pattern written down as a string, and after comma where to search in. 

	REGULAR EXPRESSIONS: Operators
	http://v4.software-carpentry.org/regexp/operators.html
	# A pattern can be written to pull out certain information
		#Example, when pulling out a date commands are the following...
			# record = 'Davison/May 22, 2010/1721.3'
			# site, date, reading = record.split('/')
			# month, day, year = date.split('/')
			# if day[-1] == ',':
			#   day = day[:-1}
			#print year, month, day
			# show_groups('(.+)/(.+) (.+),? (.{4})/(.+)'
				the curly brackets mean match the pattern exaclty 4 times against the string. 
			# at 8:03 square brackets example [0-9] are for a set of numbers 
	
	REGULAR EXPRESSIONS: Mechanics
	# http://v4.software-carpentry.org/regexp/mechanics.html

	REGULAR EXPRESSIONS: Patterns
	#http://v4.software-carpentry.org/regexp/patterns.html

	REGULAR EXPRESSIONS: More Tools
	#http://v4.software-carpentry.org/regexp/module.html


https://gist.github.com/endolith/250860
